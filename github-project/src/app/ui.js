//clase de interfaz de usuario
class UI {
    //constructor del div en el html con id profile
    constructor() {
       this.profile = document.getElementById('profile');
    }

    //obtencion de datos de perfil
    showProfile(user) {
        console.log(user)
        this.profile.innerHTML = `
            <div class="card mt-2 animated bounceInLeft">
               <img src="${user.avatar_url}" class="card-img-top"/>
               <div class="card-body">
                    <h3 class="card-title">${user.name} / ${user.login}</h3>
                    <a href="${user.html_url}" class="btn btn-primary btn-block" target="_blank">View Profile</a>
                    <span class="badge badge-success">
                        Followers: ${user.followers}
                    </span>
                    <span class="badge badge-primary">
                        Following: ${user.following}
                    </span>
                    <span class="badge badge-warning">
                        Company: ${user.caompany}
                    </span>
                    <span class="badge badge-info d-block">
                        Blog: ${user.blog}
                    </span>
               </div>

            </div>
        `;
        //borrar mensajes de error
        this.clearMessage();
    }

    //muestra mensajes de error si algun usuario no existe
    showMessage( message, cssClass ) {
        const div = document.createElement('div');
        div.className = cssClass;
        div.appendChild(document.createTextNode(message));
        const content = document.querySelector('.row');
        const profile = document.querySelector('#profile');
        content.insertBefore(div, profile);

    }

    //limpia mensajes de error
    clearMessage() {
        const alertFound = document.querySelector('.alert');
        if(alertFound) {
            alertFound.remove();
        }
    }

    //captura y muestra informacion de los repositorios
    showRepositories(repositories) {
        let template = '';
        repositories.forEach(repo => {
            template += `
                <div class="card-body mat-2 animated bounceInUp">
                    <div class="row">
                        <div class="col-md-6">
                            <a href="${repo.html_url}" target="_blank">${repo.name}</a>
                        </div>
                        <div class="col-md-6">
                            <span class="badge-badge-primary">
                                Lenguajes: ${repo.language}
                            </span>
                            <br>
                            <span class="badge-badge-primary">
                                Sizes: ${repo.size} KB
                            </span>
                            
                        </div>
                    </div>
                </div>
            `;

        });    
            
        document.getElementById('repositories').innerHTML = template;

    }
}

//exporta la clase en el documento creado
module.exports = UI;