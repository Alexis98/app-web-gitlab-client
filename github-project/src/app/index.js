//llamado a los documentos de consulta e interfaz
const UI = require('./ui');
const Github = require('./github');
const { client_id, client_secret } = require('./config.json');

//creacion de constante para las clases creadas en los otros documentos
const github = new Github(client_id, client_secret);
const ui = new UI();

//llamada al formulario
const userForm = document.getElementById('userForm');

//operacion cuando el formulario sea activado por el boton submit
userForm.addEventListener('submit', (e) => {
    
    //el escribir .value permite capturar el valor de la variable input que el usuario ingrese y no toda la etiqueta input
    const textSearch = document.getElementById('textSearch').value;
    //validacion que el formulario no este vacio
    if(textSearch !== '') {
        //obtener el dato suministrado por el usuario para realizar la busqueda
        github.fetchUser(textSearch)
        .then( data => {
            //validacion que el usuario existe
            if(data.userData.message === 'Not Found'){
                ui.showMessage('User Not Found', 'alert alert-danger mt-2 col-md-12');
            } else {
                //busqueda y devolucion de los resultados
                ui.showProfile(data.userData);
                ui.showRepositories(data.repositories);
            }  
        })
    }
    //elimina el comportamiento por defecto de un formulario para recargar la pagina
    e.preventDefault();
});