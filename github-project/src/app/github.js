//Clase gitlab
class Github {

    //constructor para el llamado de la api
    constructor( clientId, clientSecret ) {
        //validacion de credenciales
        this.client_id = clientId;
        this.client_secret = clientSecret;
        //solicitud de orden y limite en repositorios
        this.repos_count = 10;
        this.repos_sort = 'created: asc';
    }

    //consulta de la api
    async fetchUser(user) {
        //consulta a la pagina
        const userDataRequest = await fetch (`http://api.github.com/users/${user}?client_id=${this.client_id}&client_secret=${this.client_secret}`);
        const repositoriesRequest = await fetch(`http://api.github.com/users/${user}/repos?client_id=${this.client_id}&client_secret=${this.client_secret}&per_page=${this.repos_count}&sort=${this.repos_sort}`)
        //transformacion de datos a json
        const repositories = await repositoriesRequest.json()
        const userData = await userDataRequest.json();
        //retorno de datos solicitados
        return {
            userData,
            repositories
        }
    }
}

//exportancion de la clase
module.exports = Github;