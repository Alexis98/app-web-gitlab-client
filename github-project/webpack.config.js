//Dependencia
const HTMLWebpackPlugin = require('html-webpack-plugin');
//Encuentra la direccion multiplataforma
const path = require('path');

//Exporta el codigo en desarrollo a codigo de produccion que puede ser leido por un servidor
module.exports = {
    entry: './src/app/index.js',
    //direccion y archivo de produccion
    output: {
        path: path.join(__dirname,'dist'),
        filename:  'bundle.js'
    },
    //Asignacion del puerto en desarrollo
    devServer: {
        port: 3000
    },
    //Convierte el html que llama al proyecto en codigo de produccion
    plugins: [
        new HTMLWebpackPlugin({
            template: './src/index.html'
        })
    ]
}